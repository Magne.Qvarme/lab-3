package no.uib.inf101.terminal;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TestSimpleShellEcho {

  @Test
  public void testEchoInSimpleShell() {
    SimpleShell shell = new SimpleShell();
    shell.installCommand(new CmdEcho());
    shell.aKeyIsPressed('e');
    shell.aKeyIsPressed('c');
    shell.aKeyIsPressed('h');
    shell.aKeyIsPressed('o');
    shell.aKeyIsPressed(' ');
    shell.aKeyIsPressed('f');
    shell.aKeyIsPressed('o');
    shell.aKeyIsPressed('o');
    shell.aKeyIsPressed(' ');
    shell.aKeyIsPressed('b');
    shell.aKeyIsPressed('a');
    shell.aKeyIsPressed('r');
    shell.aKeyIsPressed('\n');

    String expected = "$ echo foo bar\nfoo bar \n$ ";
    assertEquals(expected, shell.whatTheScreenLooksLike());
  }
}
