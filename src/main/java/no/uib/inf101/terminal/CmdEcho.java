package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    @Override
    public String run(String[] args){
        String newString = "";
        for (String i : args){
            newString += i + " ";
        }
        return newString;
    }

    @Override
    public String getName(){
    return "echo";
    }
    
}
